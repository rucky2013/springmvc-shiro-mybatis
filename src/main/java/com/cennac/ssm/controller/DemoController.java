package com.cennac.ssm.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Created by Cennac on 16/3/2.
 */
@Controller
@RequestMapping(value = "/demo")
public class DemoController {
//    主页面
    @RequestMapping(value = "/main")
    public String main() {
        return "demo/main";
    }

//    主页面2
    @RequestMapping(value = "/main2")
    public String main2() {
        return "demo/main2";
    }

//    登录页面
    @RequestMapping(value = "/login")
    public String login() {
        return "demo/login";
    }

    //    日历
    @RequestMapping(value = "/calendar")
    public String calendar() {
        return "demo/calendar";
    }
    //    窗口小部件
    @RequestMapping(value = "/widgets")
    public String widgets() {
        return "demo/widgets";
    }

    //    图表
    @RequestMapping(value = "/chartjs")
    public String chartjs() {
        return "demo/chartjs";
    }

    //  浮选
    @RequestMapping(value = "/flot")
    public String flot() {
        return "demo/flot";
    }


    //    内联
    @RequestMapping(value = "/inline")
    public String inline() {
        return "demo/inline";
    }


    //    莫里斯
    @RequestMapping(value = "/morris")
    public String morris() {
        return "demo/morris";
    }

    //    404
    @RequestMapping(value = "/404")
    public String n404() {
        return "demo/404";
    }
    //    500
    //@RequestMapping(value = "/500")
    //public String 500() {
    //    return "demo/500";
    //}
    //    空白
    @RequestMapping(value = "/blank")
    public String blank() {
        return "demo/blank";
    }
    //    发票打印
    @RequestMapping(value = "/invoice-print")
    public String invoice_print() {
        return "demo/invoice-print";
    }
    //    发票
    @RequestMapping(value = "/invoice")
    public String invoice() {
        return "demo/invoice";
    }
    //    锁屏
    @RequestMapping(value = "/lockscreen")
    public String lockscreen() {
        return "demo/lockscreen";
    }
    //    步幅
    @RequestMapping(value = "/pace")
    public String pace() {
        return "demo/pace";
    }
    //    简介
    @RequestMapping(value = "/profile")
    public String profile() {
        return "demo/profile";
    }
    //    注册
    @RequestMapping(value = "/register")
    public String register() {
        return "demo/register";
    }

    //    高级
    @RequestMapping(value = "/advanced")
    public String advanced() {
        return "demo/advanced";
    }

    //    编辑
    @RequestMapping(value = "/editors")
    public String editors() {
        return "demo/editors";
    }

    //    一般
    @RequestMapping(value = "/general")
    public String general() {
        return "demo/general";
    }

    //    盒子
    @RequestMapping(value = "/boxed")
    public String boxed() {
        return "demo/boxed";
    }

    //    侧边栏
    @RequestMapping(value = "/collapsed-sidebar")
    public String collapsed_sidebar() {
        return "demo/collapsed-sidebar";
    }


    //    固定
    @RequestMapping(value = "/fixed")
    public String fixed() {
        return "demo/fixed";
    }

    //    顶部导航
    @RequestMapping(value = "/top-nav")
    public String top_nav() {
        return "demo/top-nav";
    }

    //    撰写
    @RequestMapping(value = "/compose")
    public String compose() {
        return "demo/compose";
    }
    //    邮箱
    @RequestMapping(value = "/mailbox")
    public String mailbox() {
        return "demo/mailbox";
    }
       //    已读邮件
    @RequestMapping(value = "/read-mail")
    public String read_mail() {
        return "demo/read-mail";
    }

    //    数据
    @RequestMapping(value = "/data")
    public String data() {
        return "demo/data";
    }
    //    简单
    @RequestMapping(value = "/simple")
    public String simple() {
        return "demo/simple";
    }




    //    按钮组
    @RequestMapping(value = "/buttons")
    public String buttons() {
        return "demo/buttons";
    }
    //    图标
    @RequestMapping(value = "/icons")
    public String icons() {
        return "demo/icons";
    }
    //    模块
    @RequestMapping(value = "/modals")
    public String modals() {
        return "demo/modals";
    }
    //    滑动条
    @RequestMapping(value = "/sliders")
    public String sliders() {
        return "demo/sliders";
    }
    //    时间表
    @RequestMapping(value = "/timeline")
    public String timeline() {
        return "demo/timeline";
    }
}
